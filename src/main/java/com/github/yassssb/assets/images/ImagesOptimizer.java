/* Copyright (c) 2018-2023 Pierre LEVY
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.github.yassssb.assets.images;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;

/**
 * ImagesOptimizer
 */
public class ImagesOptimizer 
{
    private static final String PNG_EXT = ".png";
    private static final String JPG_EXT = ".jpg";
    private static final String JPEG_EXT = ".jpeg";
    private static final String CONFIG_WATERMARK_FILE = "watermark.file";
    private static final String CONFIG_WATERMARK_DIRS = "watermark.dirs";
    
    /**
     * Process recusively an directory tree that contains PNG or JPEG images.It optimize images and copy them into an output tree
     * @param directory The current directory
     * @param strRelativePath The relative path from the root
     * @param strOutputPath the output root path
     * @param strCachePath The cache path
     * @param strRootPath The root path
     * @param config Config parameter map
     * @throws java.io.IOException if an error occurs
     */
    public static void processDirectory( File directory , String strRelativePath , String strOutputPath , String strCachePath , String strRootPath , Map< String, Object> config) throws IOException 
    {
        File[] files = directory.listFiles();
        boolean bWatermark = false;
        BufferedImage watermarkImage = null;
        
        if( config.containsKey( CONFIG_WATERMARK_FILE ) && config.containsKey( CONFIG_WATERMARK_DIRS ))
        {
           if( isWatermarkDir( directory , strRootPath , config ))
           {
               String strImageFilePath = strRootPath + File.separator + (String) config.get( CONFIG_WATERMARK_FILE );
               File watermarkImageFile = new File ( strImageFilePath );
               watermarkImage = ImageIO.read( watermarkImageFile );
               bWatermark = true;
           }
        }

        for( File file : files )
        {
            try 
            {
                if( file.isDirectory() )
                {
                    processDirectory( file , strRelativePath + file.getName() , strOutputPath , strCachePath , strRootPath , config );
                }
                else
                {
                    processFile( file, directory, strRelativePath , strOutputPath , strCachePath , bWatermark , watermarkImage );
                }
            }
            catch( IOException ex )
            {
                System.out.println( "***** Error optimizing file : " + file.getAbsolutePath()+ " Message : " + ex.getMessage() );
            }
        }
    }

    /**
     * Is the directory contains images that should be watermarked
     * @param directory The directory
     * @param strRootPath The root path
     * @param config The config map
     * @return true if it a watermark dir
     */
    private static boolean isWatermarkDir(File directory, String strRootPath , Map<String, Object> config) 
    {
        String strWatermarkDirs =  (String) config.get( CONFIG_WATERMARK_DIRS );
        String[] dirs = strWatermarkDirs.split( "," );
        for( String strPath : dirs )
        {
            strPath = strRootPath  + File.separator + strPath;
            if( directory.getAbsolutePath().equals( strPath ))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @param file
     * @param directory
     * @param strRelativePath
     * @param strOutputPath
     * @param strCachePath
     * @param bWatermark
     * @param watermarkImage
     * @throws IOException 
     */
    private static void processFile( File file, File directory, String strRelativePath , String strOutputPath , String strCachePath , boolean bWatermark , BufferedImage watermarkImage ) throws IOException 
    {
        String strCacheKey = file.getName() + file.length() + file.lastModified();
        String strCacheFilePath =  strCachePath + File.separator + strCacheKey;
        File cached = new File( strCacheFilePath );
        if( cached.exists() )
        {
            String strDest = strOutputPath + strRelativePath + File.separator + file.getName();
            FileUtils.copyFile( cached , new File( strDest ));
            System.out.println(" Copy optimized image from cache : " + file.getName() );
        }
        else 
        {
            File dest = new File( strOutputPath + strRelativePath + File.separator + file.getName() );
            if( bWatermark )
            {
                WatermarkUtils.addImageWatermark( watermarkImage , file , dest );
            }
            if( file.getName().endsWith( PNG_EXT ))
            {
                PngOptimizerService.optimize( file.getName(), directory.getAbsolutePath() , strOutputPath + strRelativePath );
            }
            else if( file.getName().endsWith( JPG_EXT ) || file.getName().endsWith( JPEG_EXT ) )
            {
                JpegOptimizerService.optimize( dest , strOutputPath + strRelativePath );
            }
            File optimized = new File( strOutputPath + strRelativePath + File.separator + file.getName() );
            FileUtils.copyFile( optimized , cached );
            System.out.println("Add file:" + optimized.getName() + " to optimized image cache");
        }
        
    }
            
    
}
