/* Copyright (c) 2018-2023 Pierre LEVY
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.yassssb.assets.images;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * WatermarkUtils
 *
 * @author pierre
 */
public class WatermarkUtils
{

    /**
     * Embeds an image watermark over a source image to produce a watermarked
     * one.
     *
     * @param watermarkImage The image file used as the watermark.
     * @param sourceImageFile The source image file.
     * @param destImageFile The output image file.
     */
    public static void addImageWatermark( BufferedImage watermarkImage, File sourceImageFile, File destImageFile)
    {
        try
        {
            BufferedImage sourceImage = ImageIO.read(sourceImageFile);

            // initializes necessary graphic properties
            if (sourceImage != null)
            {
                Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
                AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
                g2d.setComposite(alphaChannel);

                // calculates the coordinate where the image is painted
                int topLeftX = sourceImage.getWidth() - watermarkImage.getWidth();
                int topLeftY = sourceImage.getHeight() - watermarkImage.getHeight();

                // paints the image watermark
                g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);

                ImageIO.write(sourceImage, "png", destImageFile);
                g2d.dispose();

                System.out.println("The image watermark is added to the image.");
            }

        }
        catch (IOException ex)
        {
            System.err.println(ex);
        }
    }

}
